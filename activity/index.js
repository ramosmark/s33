fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((data) => {
    const todosTitle = data.map((todo) => todo.title)
    console.log(todosTitle)
})

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((data) => console.log(data))

fetch('https://jsonplaceholder.typicode.com/todos', {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        userId: 1,
        id: 201,
        title: "Created To Do List Item",
        completed: false
    })
})
.then((response) => response.json())
.then((data) => console.log(data))


fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        userId: 1,
        title: "Updated To Do List Item",
        completed: false,
        description: 'To update my todo list with a different data structure',
        status: 'Pending'
    })
})
.then((response) => response.json())
.then((data) => console.log(data))

fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        title: "Updated To Do List Item Again",
    })
})
.then((response) => response.json())
.then((data) => console.log(data))

fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'DELETE'
})